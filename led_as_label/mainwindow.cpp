#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtWidgets>

class LED : public QWidget
{
public:
    LED(QWidget * parent=0) : QWidget(parent), m_color(QColor(0,255,0)) {}

protected:
    void paintEvent(QPaintEvent *) override
    {
        QPainter painter(this);
        painter.setBrush(m_color);
        painter.drawEllipse(0, 0, 10, 10);
    }

private:
    QColor m_color;
};

class Discretes : public QHBoxLayout
{
public:
    Discretes(QWidget * parent=0) : QHBoxLayout(parent)
    {
        LED * led = new LED;
        QLabel * lbl = new QLabel("Test");
        QLabel * lbl2 = new QLabel("Test");
        setSpacing(0);
        setMargin(0);
//        setAlignment(Qt::AlignLeft);
        addWidget(led);
        addWidget(lbl);
        addWidget(lbl2);
    }
private:
//    LED * led;
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    Discretes * d = new Discretes;
    ui->widget->setLayout(d);
}

MainWindow::~MainWindow()
{
    delete ui;
}
