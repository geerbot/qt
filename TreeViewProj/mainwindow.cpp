#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVBoxLayout>
#include <QTreeWidget>
#include <QAbstractItemModel>
#include <QDebug>
#include <QObject>

//class MyTreeItem : public QAbstractItemModel
//{
//public:
//    MyTreeItem() : QAbstractItemModel()
//    {
//        new QTreeWidgetItem;
//    }

//    virtual ~MyTreeItem() {}
//};

QTreeWidgetItem * item1;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QVBoxLayout * vlayout = new QVBoxLayout;
    QTreeWidget * tree = new QTreeWidget;
    tree->setEditTriggers(QTreeWidget::NoEditTriggers);
    QStringList hdrList;
    hdrList << "Items" << "Value";
    tree->setHeaderLabels(hdrList);
    item1 = new QTreeWidgetItem(QStringList("Item 1"));
    connect(tree, SIGNAL(itemDoubleClicked(QTreeWidgetItem *, int)), this, SLOT(setRow(QTreeWidgetItem *, int)));
//    tree->itemDoubleClicked()
    item1->addChild(new QTreeWidgetItem(QStringList("Subitem 1")));
    tree->setColumnCount(2);
    tree->setColumnWidth(0, 200);
    tree->addTopLevelItem(item1);
    tree->addTopLevelItem(new QTreeWidgetItem(QStringList("Item 2")));
    vlayout->addWidget(tree);
//    QTreeView * tree = new QTreeView;
//    tree->add
//    vlayout->addWidget(tree);
    ui->centralWidget->setLayout(vlayout);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setRow(QTreeWidgetItem * item, int column)
{
//    qDebug() << "hello";
    if(column > 0)
        item->setText(column, "value");
//    item1->addChild(new QTreeWidgetItem);
}
