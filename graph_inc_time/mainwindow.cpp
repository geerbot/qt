#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtWidgets>
#include <QtCharts>
#include <iostream>

static int i = 0;
QCheckBox * button1;
QCheckBox * button2;
QCheckBox * button3;

class MyChart : public QChart
{
public:
    MyChart() : QChart()
    {
        series0 = new QLineSeries;
        series1 = new QLineSeries;
        series2 = new QLineSeries;
        if(button1->isChecked())
            *series0 << QPointF(0,0) << QPointF(i+1,i+1);
        if(button2->isChecked())
            *series1 << QPointF(0,1) << QPointF(i+1,i+2);
        if(button3->isChecked())
            *series2 << QPointF(0,2) << QPointF(i+1,i+3);
        ++i;

        legend()->hide();
        addSeries(series0);
        addSeries(series1);
        addSeries(series2);
        createDefaultAxes();
        axes(Qt::Horizontal).first()->setRange(0,10);
        axes(Qt::Vertical).first()->setRange(0,10);
    }

    ~MyChart() { qDebug() << "Chart terminated\n"; }

private:
    QLineSeries * series0;
    QLineSeries * series1;
    QLineSeries * series2;
};

class MyLayout : public QHBoxLayout
{
public:
    MyLayout(QWidget * parent=0) : QHBoxLayout(parent)
    {
        myChart = new MyChart;
        QChartView * view = new QChartView(myChart);
        addWidget(view);
    }

    ~MyLayout() { delete myChart; }

private:
    MyChart * myChart;
};

class MyButtons : public QVBoxLayout
{
public:
    MyButtons() : QVBoxLayout()
    {
        button1 = new QCheckBox();
        button2 = new QCheckBox();
        button3 = new QCheckBox();
//        button1->setAutoExclusive(true);
//        button2->setAutoExclusive(true);
//        button3->setAutoExclusive(true);
        button1->setChecked(true);
        button2->setChecked(true);
        button3->setChecked(true);
        addWidget(button1);
        addWidget(button2);
        addWidget(button3);
    }
private:

};

MyLayout * myLayout;

void MainWindow::UpdateTime()
{
    qDebug() << "Timer Event: " << i << "\n";
    if(i == 10)
    {
        i=0;
    }

    delete myLayout;
    myLayout = new MyLayout;
    ui->widget->setLayout(myLayout);
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    MyButtons * buttons = new MyButtons;
    ui->widget_2->setLayout(buttons);

    myLayout = new MyLayout;
    ui->widget->setLayout(myLayout);

    timer = new QTimer(this);
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(UpdateTime()));
    timer->start(1000);
}

MainWindow::~MainWindow()
{
    delete ui;
}
