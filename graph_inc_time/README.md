# Graph Incrementing Time

## Overview
Simple C++ based Qt GUI that shows graphing over time with selectable lines

![results](qt_graph_inc_time.gif)
