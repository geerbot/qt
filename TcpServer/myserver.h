#ifndef MYSERVER_H
#define MYSERVER_H
#include <QTcpServer>
#include <QTcpSocket>

class MyServer : public QObject
{
public:
    MyServer(QObject * parent=0);

public slots:
    void newConnection();

private:
    QTcpServer * server;
};

#endif // MYSERVER_H
