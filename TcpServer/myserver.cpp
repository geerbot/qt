#include "myserver.h"

MyServer::MyServer(QObject * parent) : QObject(parent)
{
    server = new QTcpServer(this);
    connect(server, SIGNAL(&QTcpServer::newConnection)), this, SLOT(&MyServer::newConnection()));
    if(!server->listen(QHostAddress::Any, 8000)) {
        qDebug() << "Error starting server: " << server->errorString() << "\n";
    }
}

void MyServer::newConnection() {
    QTcpSocket * socket = server->nextPendingConnection();
    if(server->waitForNewConnection(5000)) {
        qDebug() << "Found client\n";
        qDebug() << socket->read(1024);
    }
}
