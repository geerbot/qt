#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtCharts>
#include <QtCharts/QChartView>

class Graph : public QHBoxLayout
{
public:
    Graph(GraphData * series) : QHBoxLayout()
    {
        chart = new QChart;
        chart->legend()->hide();
        chart->addSeries(series);

        QValueAxis * yAxis = new QValueAxis;
        yAxis->setTitleText("Value");
        yAxis->setRange(0, series->getTicks());
        yAxis->setTickCount(series->getTicks());
        chart->addAxis(yAxis,Qt::AlignLeft);

        QValueAxis * xAxis = new QValueAxis;
        xAxis->setTitleText("Time");
        xAxis->setRange(series->getCnt()-series->getLength(), series->getCnt());
        xAxis->setTickCount(series->getTicks());
        chart->addAxis(xAxis,Qt::AlignBottom);

        view = new QChartView(chart);
        addWidget(view);
    }

private:
    QChart * chart;
    QChartView * view;
};

Graph * graph;
void MainWindow::update()
{
    delete graph;
    series->update();
    graph = new Graph(series);
    ui->widget->setLayout(graph);
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    series = new GraphData;

    timer = new QTimer;
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(1000);

    series = new GraphData;
    graph = new Graph(series);
    ui->widget->setLayout(graph);
}

MainWindow::~MainWindow()
{
    delete ui;
}
