#include "graphdata.h"

#define seriesLength 10

GraphData::GraphData() : QLineSeries(), cnt(0), y(1), dir(DIR_UP)
{
    append(cnt,y);
    ++cnt;
    ++y;
}

int GraphData::getTicks() { return seriesLength; }
void GraphData::update()
{
    append(cnt, y);
    ++cnt;
    if (dir == DIR_UP) { ++y; }
    if (dir == DIR_DOWN) { --y; }
    if (y == seriesLength) { dir = DIR_DOWN; }
    if (y == 0) { dir = DIR_UP; }
    if (cnt >= seriesLength) { remove(0); }
}
