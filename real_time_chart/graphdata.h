#ifndef GRAPHDATA_H
#define GRAPHDATA_H
#include <QtCharts>
#include <QtCharts/QLineSeries>

class GraphData : public QLineSeries
{
public:
    GraphData();

    int getLength() { return count(); }
    int getCnt() { return cnt; }
    int getTicks();

public slots:
    void update();

private:
    int cnt;
    int y;

    typedef enum
    {
        DIR_UP=0,
        DIR_DOWN
    } DIR;
    DIR dir;
};

#endif // GRAPHDATA_H
