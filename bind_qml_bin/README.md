# Bind QML and C++

## Overview
Simple application that uses QML inside a C++ project. To demo this it has a light/dark mode button.

![result](qt_bind_qml_c++.gif)
