import QtQuick 2.0
import QtQuick.Controls 2.0

Item {
    id: root
    width: parent.width
    height: parent.height
    visible: true

    Rectangle {
        property bool isDarkMode: false
        id: background
        width: parent.width
        height: parent.height
        color: "#F5F5F5"
    }

    Button {
        id: buttonMode
        anchors.horizontalCenter: parent.horizontalCenter

        Text {
            id: buttonText
            text: "To Dark"
            anchors.centerIn: parent
        }

        onReleased: {
            buttonText.text =  background.isDarkMode ? "To Dark" : "To Light"
            background.color =  background.isDarkMode ? "#F5F5F5" : "#505050"
            background.isDarkMode = background.isDarkMode ? false : true
        }
    }

    TextField {
        text: "hello"
        placeholderText: qsTr("User name")
        anchors.centerIn: parent

        onTextChanged: backend.userName = text
    }
}
