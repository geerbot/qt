#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QQmlApplicationEngine>
#include <QLayout>
#include <QQuickView>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QHBoxLayout * w = new QHBoxLayout;
    w->setMargin(0);
    QQuickView * view = new QQuickView;
    view->setSource(QUrl(QStringLiteral("../main.qml")));
    QWidget * container = QWidget::createWindowContainer(view, this);
    w->addWidget(container);
    ui->centralWidget->setLayout(w);
}

MainWindow::~MainWindow()
{
    delete ui;
}
