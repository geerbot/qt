import sys
from PySide2.QtWidgets import QApplication, QPushButton

def say_hello():
    print('Button clicked')

app=QApplication(sys.argv)
button=QPushButton('Click me')
button.clicked.connect(say_hello)
button.show()
app.exec_()
