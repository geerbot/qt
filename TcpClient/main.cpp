#include <QCoreApplication>
#include <QTcpSocket>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QTcpSocket * socket = new QTcpSocket;
    socket->connectToHost("127.0.0.1",8000);
    if(socket->waitForConnected(5000)) {
        qDebug() << "Connected\n";
        qDebug() << "Send " << socket->write("Hello World!") << " bytes\n";
    }
    socket->disconnectFromHost();
    qDebug() << "done\n";

    return a.exec();
}
