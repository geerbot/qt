import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *

vertices=(
        (1,-1,-0.5),
        (1,1,-0.5),
        (-1,1,-0.5),
        (-1,-1,-0.5),
        (1,-1,0.5),
        (1,1,0.5),
        (-1,-1,0.5),
        (-1,1,0.5)
        )

vertices2=(
        (1,1,-0.5),
        (-1,1,-0.5),
        (-1,1,0.5),
        (1,1,0.5),
        (0,2,0)
        )

vertices3=(
        (1,-1,-0.5),
        (-1,-1,-0.5),
        (-1,-1,0.5),
        (1,-1,0.5),
        (0,-2,0)
        )

edges=(
        (0,1),
        (0,3),
        (0,4),
        (2,1),
        (2,3),
        (2,7),
        (6,3),
        (6,4),
        (6,7),
        (5,1),
        (5,4),
        (5,7)
        )

edges2=(
        (0,1),
        (0,3),
        (0,4),
        (2,1),
        (2,3),
        (2,4),
        (1,4),
        (3,4)
        )

def Cube():
    glBegin(GL_LINES)
    for edge in edges:
        for vertex in edge:
            glVertex3fv(vertices[vertex])
    glEnd()

def Pyramid(vert):
    glBegin(GL_LINES)
    for edge in edges2:
        for vertex in edge:
            glVertex3fv(vert[vertex])
    glEnd()

if __name__ == "__main__":
    pygame.init()
    display=(800,600)
    pygame.display.set_mode(display, DOUBLEBUF | OPENGL)

    gluPerspective(45, (display[0]/display[1]), 0.1, 50.0)

    glTranslatef(0.0, 0.0, -10)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        #glRotatef(1,3,1,1)
        glRotatef(1,0,1,0)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        Cube()
        Pyramid(vertices2)
        Pyramid(vertices3)
        pygame.display.flip()
        pygame.time.wait(10)

