#ifndef MYROUTERRULE_H
#define MYROUTERRULE_H

#include <QtHttpServer/QHttpServerRouterRule>
#include "myrouterrule_p.h"

#define PORT 8000

class Q_HTTPSERVER_EXPORT MyRouterRule : public QHttpServerRouterRule
{
    Q_DECLARE_PRIVATE(MyRouterRule)

public:
    MyRouterRule(const QString &pathPattern, const char *methods, RouterHandler &&routerHandler);

    MyRouterRule(const QString &pathPattern, RouterHandler &&routerHandler);

    virtual bool matches(const QHttpServerRequest &request, QRegularExpressionMatch *match) const;


    static QString result();
//    const char * getResponse();

    ~MyRouterRule() {}

//    MyRouterRule(MyRouterRulePrivate *d) : QHttpServerRouterRule(d->pathPattern, d->methods, std::forward<RouterHandler>(d->routerHandler)), d_ptr(d) { qDebug() << "assignment"; }
    MyRouterRule(QHttpServerRouterRulePrivate *d) : QHttpServerRouterRule(d) { qDebug() << "assignment"; }

private:
    Q_DISABLE_COPY(MyRouterRule)

//    QScopedPointer<QHttpServerRouterRulePrivate> d_ptr;
    QScopedPointer<MyRouterRulePrivate> d_ptr;

    friend class QHttpServerRouter;
};

#endif // MYROUTERRULE_H
