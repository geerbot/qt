#ifndef MYROUTERRULE_P_H
#define MYROUTERRULE_P_H

#include <QtHttpServer/qhttpserverrequest.h>
#include <QtHttpServer/qhttpserverrouterrule.h>
#include <QRegularExpression>

class MyRouterRulePrivate
{
public:
    QString pathPattern;
    QHttpServerRequest::Methods methods;
    QHttpServerRouterRule::RouterHandler routerHandler;

    QRegularExpression pathRegexp;

    static std::string result;
};

#endif // MYROUTERRULE_P_H
