#include "myrouterrule.h"
#include <QHttpServer>

static QString response;

MyRouterRule::MyRouterRule(const QString &pathPattern, const char *methods, RouterHandler &&routerHandler) :
    QHttpServerRouterRule(pathPattern, methods, std::forward<RouterHandler>(routerHandler)), d_ptr()
{
}

MyRouterRule::MyRouterRule(const QString &pathPattern, RouterHandler &&routerHandler) :
            QHttpServerRouterRule(pathPattern, QHttpServerRequest::Method::All, std::forward<RouterHandler>(routerHandler))
{
    response = "nothing";
}

QString MyRouterRule::result() { return response; }

bool MyRouterRule::matches(const QHttpServerRequest &request, QRegularExpressionMatch *match) const
{
    Q_D(const MyRouterRule);

    bool res = QHttpServerRouterRule::matches(request, match);
    if(res == false) {
        int offs = request.url().toString().toStdString().find(std::to_string(PORT)) + std::to_string(PORT).length() + 1;
        if(offs != std::string::npos) {
            std::string uri = &request.url().toString().toStdString()[offs];
            if(uri.compare(0, 6, "status") == 0) {
                uri = &uri[6];
                if(uri.compare(0, 3, "?q=") == 0) {
                    QString field = &uri[3];
                    QStringList val = field.split("test", QString::KeepEmptyParts);
                    response = "Looking for test number " + val[1];
                    res = true;
                }
            }
        }
    }
    return res;
}
