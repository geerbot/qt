#include <QCoreApplication>
#include <QtHttpServer>
#include "myrouterrule.h"

int main(int argc, char * argv[]) {
    QCoreApplication app(argc, argv);
    QHttpServer srv;
    srv.route<MyRouterRule>("/", [](){ return MyRouterRule::result(); } );
//    srv.route("/?q=arm_manager", [](){return "Hello MyRule";});
    srv.listen(QHostAddress::Any, PORT);
    return app.exec();
}
