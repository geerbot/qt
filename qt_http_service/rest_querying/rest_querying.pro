INCLUDEPATH+= \
    ../qthttpserver/include \
    ../qthttpserver/include/QtHttpServer

LIBS+=-L../qthttpserver/lib -lQt5HttpServer

SOURCES += \
    main.cpp \
    myrouterrule.cpp

HEADERS += \
    myrouterrule.h \
    myrouterrule_p.h
