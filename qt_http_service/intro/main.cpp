#include <QtHttpServer>

int main(int argc, char * argv[])
{
	QCoreApplication app(argc, argv);
	QHttpServer httpServer;
	httpServer.route("/", QHttpServerRequest::Method::Get, []() {
		return "Hello World";
	});
	httpServer.route("/", QHttpServerRequest::Method::Post, []() {
		return "Hello World Post";
	});
	httpServer.listen(QHostAddress::Any);
	return app.exec();
}
