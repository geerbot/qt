#include <QCoreApplication>
#include <QtCore>

class FooPrivate
{
public:
    FooPrivate(int _a, int _b, int _c) { a=_a; b=_b; c=_c; }
    int a;
    int b;
    int c;
};

class Foo
{
    Q_DECLARE_PRIVATE(Foo)

public:
    Foo() : d_ptr(new FooPrivate(1,2,3)) {}

    virtual void print_all() {
        Q_D(Foo);

        qDebug() << "a: " << d->a << ", b: " << d->b << ", c: " << d->c;
    }
private:
    QScopedPointer<FooPrivate> d_ptr;
};

int main(int argc, char *argv[])
{
    Foo foo;
    foo.print_all();
    return 0;
}
