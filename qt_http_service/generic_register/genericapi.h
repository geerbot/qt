#ifndef GENERICAPI_H
#define GENERICAPI_H

#include <QtHttpServer>
#include "test.h"
#include <string>

#define PORT 8000

class GenericApi
{
public:
    GenericApi();
    virtual ~GenericApi();

    char * route(const QHttpServerRequest &req);
    char * route();
    void insert(Test * test);

private:
    std::map<std::string, Test *> m_route;
};

#endif // GENERICAPI_H
