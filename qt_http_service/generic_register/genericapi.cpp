#include "genericapi.h"

GenericApi::GenericApi()
{
}

GenericApi::~GenericApi()
{
}

void GenericApi::insert(Test * test)
{
    m_route[test->getKey()] = test;
}

std::string parseUrl(QUrl url) {
    size_t beg = url.toString().toStdString().find(std::to_string(PORT)) + std::to_string(PORT).length() + 1;
    return &url.toString().toStdString()[beg];
}

char * GenericApi::route(const QHttpServerRequest &req)
{
    std::string uri = parseUrl(req.url());
    qDebug() << "get uri " << uri.c_str();
    if(m_route.find(uri) != m_route.end())
    {
        switch(req.method()) {
        case QHttpServerRequest::Method::Get:
            return m_route[uri]->route_get();
            break;
        case QHttpServerRequest::Method::Put:
            return m_route[uri]->route_put();
            break;
        case QHttpServerRequest::Method::Post:
            return m_route[uri]->route_post();
            break;
        case QHttpServerRequest::Method::Delete:
            return m_route[uri]->route_delete();
            break;
        }
    }
    return "Error";
}
