#include "test.h"
#include <stdio.h>

Test::Test() : key("test")
{
}

char * Test::route_get() {
    sprintf(str, "Hello from %s", key.c_str());
    return str;
}

char * Test::route_put() {
    sprintf(str, "Hello from %s", key.c_str());
    return str;
}

char * Test::route_post() {
    sprintf(str, "Hello from %s", key.c_str());
    return str;
}

char * Test::route_delete() {
    sprintf(str, "Hello from %s", key.c_str());
    return str;
}
