#ifndef TEST_H
#define TEST_H

#include <map>

class Test
{
public:
    Test();

    std::string getKey() {return key;}
    virtual char * route_get();
    virtual char * route_put();
    virtual char * route_post();
    virtual char * route_delete();

    std::string key;
    char str[128];
};

#endif // TEST_H
