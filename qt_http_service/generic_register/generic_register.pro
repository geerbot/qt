INCLUDEPATH+=../qthttpserver/include \
			../qthttpserver/include/QtHttpServer

LIBS+=-L../qthttpserver/lib -lQt5HttpServer

SOURCES+=main.cpp \
    genericapi.cpp \
    test.cpp \
    testcase1.cpp

HEADERS += \
    genericapi.h \
    test.h \
    testcase1.h
