#include "genericapi.h"
#include "testcase1.h"

GenericApi * generic;

int main(int argc, char * argv[])
{
	QCoreApplication app(argc, argv);

    QHttpServer httpServer;
    generic = new GenericApi;
    generic->insert(new Test);
    generic->insert(new TestCase1);
    httpServer.route("/", [](const QHttpServerRequest &req) { return generic->route(req); });
    httpServer.route("/<arg>", [](QString s, const QHttpServerRequest &req) { return generic->route(req); });
    httpServer.route("/<arg>/<arg>", [](QString s1, QString s2, const QHttpServerRequest &req) { return generic->route(req); });
    httpServer.listen(QHostAddress::Any, PORT);

	return app.exec();
}
