#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <iostream>

typedef struct
{
    char fieldA[16];
    char fieldB[16];
    int fieldC;
} MyObject;

#define setMyObjectString(obj, field, val) { strncpy(obj.field, val, sizeof(obj.field)); }
#define setMyObjectInt(obj, field, val) { obj.field = val; }

void print_myobject(MyObject obj) {
    printf("fieldA %s\n", obj.fieldA);
    printf("fieldB %s\n", obj.fieldB);
    printf("fieldC %d\n", obj.fieldC);
}

int main(int argc, char *argv[])
{
    MyObject obj1, obj2;
    QJsonObject main, details;
    QJsonDocument doc;

    setMyObjectString(obj2, fieldA, "test A");
    setMyObjectString(obj2, fieldB, "test B");
    setMyObjectInt(obj2, fieldC, 3);
    details.insert("fieldA", obj2.fieldA);
    details.insert("fieldB", obj2.fieldB);
    details.insert("fieldC", obj2.fieldC);

    main.insert("name","myjsondoc");
    main.insert("details", details);

    doc.setObject(main);
    std::cout << doc.toJson().toStdString();

    return 0;
}
