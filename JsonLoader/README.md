# JSON Loader

## Overview
Use Qt JSON libraries to build file using QJsonDocument, QJsonObject, insert

## Commands
QJsonObject::insert()
QJsonDocument::setDocument()
QJsonDocument::toJson()

## Result
{
    "details": {
        "fieldA": "test A",
        "fieldB": "test B",
        "fieldC": 3
    },
    "name": "myjsondoc"
}
