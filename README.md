# Qt Projects for Learning

## JsonLoader
Use Qt JSON libraries to build file using QJsonDocument, QJsonObject, insert

## pyside
Simple Python GUIs one with button clicking

## pygl
Simple python GUI using open gl

## qt_http_service
* intro - simple hello world usage of qthttpserver
* generic_register - simple abstract class that will register endpoints without hard-coding URLs
* rest_querying - inherit routerrule to match query, then return static response

## real_time_chart
Simple C++ based Qt GUI that acts somewhat like an oscilloscope

## led_as_label
Simple C++ based GUI that inherits QWidget and QPainter to make simple LED mechanism

## graph_inc_time
Simple C++ based Qt GUI that shows graphing over time with selectable lines

## TreeViewProj
Simple C++ based Qt GUI that display table as somewhat editable tree

## Bind QML and C++
Simple C++ based QT GUI that uses QML
